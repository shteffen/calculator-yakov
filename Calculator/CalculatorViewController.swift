//
//  ViewController.swift
//  Calculator
//
//  Created by yakov shteffen on 14/02/2018.
//  Copyright © 2018 yakov shteffen. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {
    
    @IBOutlet weak var display: UILabel!
    @IBOutlet weak var operationsDisplay: UILabel!
    @IBOutlet weak var memoryDisplay: UILabel!
    private let displayFormatter = NumberFormatter()
    private var userIsInTheMiddleOfTyping = false
    private var brain = CalculatorBrain() //model
    
    @IBAction func touchBackspace(_ sender: UIButton) {
        if userIsInTheMiddleOfTyping {
            if display.text!.count > 1 {
                display.text!.removeLast()
            } else if display.text!.count == 1 {
                display.text! = "0"
                userIsInTheMiddleOfTyping = false
            }
        } else {
            brain.undo()
            if brain.resultIsPending {
                operationsDisplay.text = brain.getDescription() + " ..."
            } else {
                operationsDisplay.text = brain.getDescription() + " ="
            }
        }
    }
    
    @IBAction func touchDigit(_ sender: UIButton) {
        let digit = sender.currentTitle!
        if userIsInTheMiddleOfTyping {
            let textCurrentlyInDisplay = display.text!
            if digit == "." {
                if !textCurrentlyInDisplay.contains(".") { //adding "." is possible only if there are no decimal separators in display
                    display.text = textCurrentlyInDisplay + digit
                }
            } else {
                display.text = textCurrentlyInDisplay + digit
            }
        } else {
            if digit == "." { //special case if user presses "." before entering any numbers
                display.text = "0."
            } else {
                display.text = digit
            }
            userIsInTheMiddleOfTyping = true
        }
    }
    
    var displayValue: Double {
        get {
            return Double(display.text!)!
        }
        set {
            displayFormatter.minimumIntegerDigits = 1
            displayFormatter.maximumFractionDigits = 6
            display.text = displayFormatter.string(from: NSNumber.init(value: newValue))
        }
    }
    
    @IBAction func performMemoryOperation(_ sender: UIButton) {
        userIsInTheMiddleOfTyping = false
        let action = sender.currentTitle
        if action == "M" {
            brain.setOperand(variable: "M")
            operationsDisplay.text = brain.getDescription() + " ..."
        } else { //action == →M
            memoryDisplay.text = "M=" + display.text!
            displayValue = brain.evaluate(using: ["M" : displayValue]).result ?? 0
            operationsDisplay.text = brain.getDescription() + " ="
        }
    }
    
    @IBAction func performOperation(_ sender: UIButton) {
        
        if userIsInTheMiddleOfTyping {
            brain.setOperand(displayValue)
            userIsInTheMiddleOfTyping = false
        }
        if let mathematicalSymbol = sender.currentTitle {
            brain.performOperation(mathematicalSymbol)
            if brain.resultIsPending {
                operationsDisplay.text = brain.getDescription() + " ..."
            } else {
                operationsDisplay.text = brain.getDescription() + " ="
            }
            //NaN and +/- infinity errors
            if let errDesc = brain.errorDescription {
                operationsDisplay.text = errDesc
            }
        }
        if let result = brain.result {
            displayValue = result
        }
        if sender.currentTitle != nil && sender.currentTitle == "C" {
            display.text = "0"
            operationsDisplay.text = " "
            userIsInTheMiddleOfTyping = false
            memoryDisplay.text = "M="
            brain.clear()
        }
    }
}

